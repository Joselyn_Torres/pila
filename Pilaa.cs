﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JoselynTorres3BPila
{
    public class Pilaa
    {

        private Nodo cima = new Nodo();

        public Pilaa()
        {
            cima = null;
        }

        public void Apilar()
        {
            Nodo nuevonodo = new Nodo();
            Console.WriteLine("\nIngrese los datos:\n");
            nuevonodo.Dato = (Console.ReadLine());

            nuevonodo.Siguiente = cima;
            cima = nuevonodo;
        }

        public void Desapilar()
        {
            Nodo Actual = new Nodo();
            Actual = cima;

            if (cima != null)
            {
                while (Actual != null)
                {
                    Console.WriteLine(" " + Actual.Dato);
                    Actual = Actual.Siguiente;
                }
            }
            else
            {
                Console.WriteLine("La pila está vacía");
            }
        }
    }
}
