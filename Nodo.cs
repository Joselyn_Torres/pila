﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JoselynTorres3BPila
{
    public class Nodo
    {
        public string Dato;
        public Nodo siguiente;

        public string getdato()
        {
            return Dato;
        }
        public void setdato(string Dato)
        {
            this.Dato = Dato;
        }
        public Nodo Siguiente
        {
            get { return siguiente; }
            set { siguiente = value; }
        }
    }
}
