﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JoselynTorres3BPila
{
    public class Program
    {
        static void Main(string[] args)
        {
            Pilaa pilita = new Pilaa();
            pilita.Apilar();
            pilita.Apilar();
            pilita.Apilar();

            Console.WriteLine("\n Desapilar datos:\n");

            pilita.Desapilar();
        }
    }
}